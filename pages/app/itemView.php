<?php
	if($_GET){
        $itemId = $_GET['itemId'] ? $_GET['itemId'] : '';
	}else{
        $itemId = '';
    }
?>
<script type="text/javascript">
    <?php
        echo "var itemId = {$itemId};";
    ?>
</script>
<script src="assets/js/app/itemView.js"></script>
<div class="container">
    <div class="d-flex flex-row">
       <div class="flex-row-auto" style="width:70%">
            <div class="card card-custom card-stretch">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col">
                            <div id="itemName" name="itemName" style="font-size:26px;">Web Application</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <div class="">
                                <img id="itemImg" name="itemImg" style="width:90%;max-height:500px;" alt="Pic" src="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <div style="font-size:20px;"><span style="border-bottom: 6px solid #179f00;">Description</span></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col">
                            <div id="itemDescription" name="itemDescription" style="font-size:16px;">Web Application Test</div>
                        </div>
                    </div>
                </div>
            </div>         
       </div> 
       &nbsp;
       <div class="flex-row-fluid">
           <div class="card card-custom card-stretch">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col" style="text-align: center;">
                            <div class="image-input image-input-outline" style="background-image: url(assets/media/users/blank.png)">
                                <div id="userImg" name="userImg" class="image-input-wrapper" style="background-image: url(assets/media/users/300_21.jpg)"></div>
                            </div>
                            <span id="userName" name="userName" class="form-text" style="color:#179f00; font-size:17px; font-weight:bold;">Profile Name</span>
                            <span id="userAddress" name="userAddress" class="form-text" style="font-size:15px;">Address</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col" style="text-align: center;">
                            <a id="userPhoneWAhref" name="userPhoneWAhref" href="https://api.whatsapp.com/send?phone=+919890860909" class="btn btn-outline-success" style="width:100%; text-align:left; border-color:#179f00; color:black; font-weight:bold;" onMouseOver="this.style.backgroundColor ='#179f00'" onMouseOut="this.style.backgroundColor ='white'" >
                                <i class="fab fa-whatsapp-square" style="font-size: 36px; color: #25d366"></i>
                                <span id="userPhoneWA" name="userPhoneWA">+919890860909</span>
                            </a>
                        </div>
                    </div>                        
                    <div class="form-group row">
                        <div class="col" style="text-align: center;">
                            <a id="userPhonehref" name="userPhonehref" href="tel:+919890860909" class="btn btn-outline-success" style="width:100%; text-align:left; border-color:#179f00; color:black; font-weight:bold;" onMouseOver="this.style.backgroundColor ='#179f00'" onMouseOut="this.style.backgroundColor ='white'" >
                                <i class="fa fa-phone-alt" style="font-size: 26px; color: #000000"></i>
                                <span id="userPhone" name="userPhone">+919890860909</span>
                            </a>
                        </div>
                    </div>   
                    <div class="form-group row">
                        <div class="col" style="text-align: center;">
                            <a id="userEmailhref" name="userEmailhref" href="mailto:patel.dilip@rushabhbrainstormers.com" class="btn btn-outline-success" style="width:100%; text-align:left; border-color:#179f00; color:black; font-weight:bold;" onMouseOver="this.style.backgroundColor ='#179f00'" onMouseOut="this.style.backgroundColor ='white'" >
                                <i class="fa fa-envelope" style="font-size: 36px; color: #1a73e8"></i>
                                <span id="userEmail" name="userEmailspan">patel.dilip@rushabhbrainstormers.com</span>
                            </a>
                        </div>
                    </div>  
                    <hr>   
                    <div class="form-group row">
                        <div class="col">
                            <i class="fas fa-calendar-alt"></i>
                            <span>Posted on:</span>
                            <span id="userPostedOn" name="userPostedOn" style="float:right;">11-12-2020</span>
                        </div>
                    </div>   
                    <div class="form-group row">
                        <div class="col">
                            <i class="fas fa-user"></i>
                            <span>User Type:</span>
                            <span id="userType" name="userType" style="float:right;">Agent</span>
                        </div>
                    </div>                                               
                    <hr>                                             
                </div>
           </div>
       </div>        
    </div>
    <div class="d-flex flex-row">
        <div class="card" style="width:100%;">
            <div class="card-header">
                <span style="font-size:20px;border-bottom: 6px solid #179f00;">Related Leads</span>
            <div>
            <div class="card-body">
                <div id="latestLeadsDiv" class="d-flex flex-center align-items-center">
                </div>
            </div>
        </div>
    </div>            
</div>