<div class="row">
    <div class="col-lg-1"></div>
        <div class="col-lg-11" >
        <h3><span style=" border-bottom: 6px solid #179f00;">My Accounts</span></h3>
    </div>
</div>
</br>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-3">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">User Information</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row" style="text-align: center;">
                    <div class="col-lg-15 col-xl-8">
                        <div id="profileImgDiv" class="image-input image-input-empty image-input-outline">
                            <div class="image-input-wrapper"></div>
                        </div>
                        </br>
                        <span class="form-text text-muted"><span id="userProfileName" style="font-weight:bold; font-size:15px;"></span></span> 
                        </br>
                        <button onClick="editUserProfile();" type="button" class="btn btn-primary" style="background-color: #179f00;">Edit Profile</button>
                    </div>
                </div>  
                </br>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <i class="icon-lg text-dark-50 flaticon2-calendar-9"></i> Registered On:
                    </div>
                    <label class="col-lg-6 col-form-label text-right"><span id="dateRegisterdOn"></span></label>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                    <i class="icon-lg text-dark-50 flaticon2-avatar"></i> User Type:
                    </div>
                    <label class="col-lg-6 col-form-label text-right"><span id="userType"></span></label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-8">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Leads Details
                    </h3>
                </div>
            </div>
            <div class="card-body" id="userLeadDetailsCard">
            </div> 
            </div>
        </div>
    </div>
</div>
<?php include("pages/app/confirmPopup.html"); ?>
<script src="assets/js/app/userDetails.js"></script>	