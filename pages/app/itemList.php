<?php
	if($_GET){
        $reqOrigin = $_GET['reqOrigin'] ? $_GET['reqOrigin'] : '';
        if(isset($_GET['categoryId'])){
			$categoryId = $_GET['categoryId'] ? $_GET['categoryId'] : '';
		}else{
			$categoryId = '';
		}	
        if(isset($_GET['searchText'])){
			$searchText = $_GET['searchText'] ? $_GET['searchText'] : '';
		}else{
			$searchText = '';
		}	
	}else{
        $reqOrigin = '';
        $categoryId = '';
        $searchText = '';
    }
?>

<script type="text/javascript">
    <?php
        echo "var reqOrigin = {$reqOrigin};";
        echo "var categoryId = '{$categoryId}';";
        echo "var searchText = '{$searchText}';";
    ?>
</script>

<div class="container">
    <div class="card-header">
        <span id="itemListTitleSpan" style="border-bottom: 6px solid #179f00;" class="card-label font-size-h2 font-weight-bolder text-dark">Item List</span>
    </div>     
    </br>
    <div class="d-flex flex-row">
        <div class="flex-column offcanvas-mobile w-300px w-xl-325px" id="kt_profile_aside">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-3">
                        <span class="card-label font-size-h3 font-weight-bolder text-dark">Refine Search</span>
                    </h3>
                </div>
                <div class="card-body pt-4">
                    <form>
                        <div class="mt-6">
                            <div class="form-group mb-8">
                                <label class="font-weight-bolder">Keyword</label>
                                <input id="keyword" name="keyword" type="text" class="form-control form-control-solid form-control-lg" placeholder="">
                            </div>
                            <div class="form-group mb-8">
                                <label class="font-weight-bolder">Category</label>
                                <select id="category" name="category" class="form-control form-control-solid form-control-lg selectpicker" data-live-search="true">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group mb-8">
                                <label class="font-weight-bolder">Country</label>
                                <select  id="country" name="country" class="form-control form-control-solid form-control-lg selectpicker" data-live-search="true">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bolder">User Type</label>	
                                <div class="checkbox-list">
                                    <label class="checkbox">
                                        <input type="checkbox" id="usrTypeA" name="Checkboxes1">
                                        <span></span>Agent
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" id="usrTypeT" name="Checkboxes1">
                                        <span></span>Trader
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" id="usrTypeM" name="Checkboxes1">
                                        <span></span>Manufacturer
                                    </label>
                                </div>
                                </br>
                                <label class="font-weight-bolder">Post Type</label>	
                                <div class="checkbox-list">
                                    <label class="checkbox">
                                        <input id="postTypeS" type="checkbox" name="Checkboxes1">
                                        <span></span>Selling
                                    </label>
                                    <label class="checkbox">
                                        <input id="postTypeB"  type="checkbox" name="Checkboxes1">
                                        <span></span>Buying
                                    </label>
                                    <label class="checkbox" style="display:none;">
                                        <input id="postTypeBoth" type="checkbox" name="Checkboxes1">
                                        <span></span>Both
                                    </label>
                                </div>
                            </div>
                            <button type="button" onClick="repostRefineSearch();" id="btnRefineSearch" name="btnRefineSearch" class="btn btn-primary font-weight-bolder mr-2 px-8" style="background-color:#28a745;">Refine Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="itemListDiv" class="flex-row-fluid ml-lg-8">
        </div>
    </div>
</div>
<script src="assets/js/app/itemList.js"></script>	
