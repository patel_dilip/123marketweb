<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom" style="width:30%;margin:auto;">
            <div class="card-header" style="align-self: center;">
                <h3 class="card-title">
                    Forgot Password
                </h3>
            </div>
            <form>
            <div class="card-body">
                <div class="form-group">
                    <input type="text" class="form-control"  placeholder="Username"/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <button onClick="javascript:location.href = 'index.php'" type="submit" class="form-control" style="background-color:#179f00; color:white;font-size:15px;">RESET PASSWORD</button>
                </div>                
            </div>
            <div class="card-footer" style="text-align:center;">
                <a href="?page=login" style="color:#179f00; font-size:14px;">Login Here</a>
                </br>
                <a href="?page=register" style="color:#179f00; font-size:14px;">Register Here</a>
            </div>
            </form>
        </div>
    </div>
</div>