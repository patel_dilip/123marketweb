<script src="assets/js/app/register.js"></script>
<style>
.btn {
    font-size: 0.9rem;
}
</style>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom" style="width:60%;margin:auto;">
            <div class="card-header" style="align-self: center;">
                <h3 id="pageTitleUserInfo" class="card-title" >
                    User Registration
                </h3>
            </div>
            <form class="form" id="user_profile_form">
            <div class="card-body">
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">User Name/Email <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="username" id="username" type="text" />
                    </div>
                </div>
                <div class="row form-group" id="passwordDiv">
                    <label class="col-lg-3 col-form-label">Password <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="password" id="password" type="password" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">User Type</label>
                    <div class="col-lg-6">
                        <select id="userType" class="form-control form-control-lg form-control-solid selectpicker">
                            <option value="Agent">Agent</option>
                            <option value="Trader">Trader</option>
                            <option value="Manufacturer">Manufacturer</option>
                        </select> 
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Company Name <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="companyName" id="companyName" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Products</label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="products" id="products" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Address</label>
                    <div class="col-lg-6">
                        <textarea class="form-control form-control-lg form-control-solid" name="address" id="address" rows="3" ></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">PO. Box/Zip Code</label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="pincode" id="pincode" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Country <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <select name="country" id="country" class="form-control form-control-lg form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>  
                    </div>                    
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">State</label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" id="state" name="state" type="text" list="stateList"/>
                        <datalist id="stateList">
                            <option></option>
                        </datalist>   
                    <!--
                        <select name="state" class="form-control form-control-lg form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>    
                    -->                                                
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">City <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" id="city" name="city" type="text" list="cityList"/>
                        <datalist id="cityList">
                            <option></option>
                        </datalist>
                    <!-- 
                        <select name="city" class="form-control form-control-lg form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select> 
                    -->                                                   
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Contact Person</label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="contactPerson" id="contactPerson" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Mobile No <span class="text-danger">*</span></label>
                    <div class="col-lg-3">
                        <select title="Select Country Code" name="phoneCC" id="phoneCCM" class="form-control form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>  
                    </div>                    
                    <div class="col-lg-4">
                        <input class="form-control form-control-lg form-control-solid number" name="mobileNo" id="mobileNo" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">WhatsApp No</label>
                    <div class="col-lg-3">
                        <select title="Select Country Code" name="phoneCC" id="phoneCCWA" class="form-control form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>  
                    </div>                    
                    <div class="col-lg-4">
                        <input class="form-control form-control-lg form-control-solid number" name="whatsAppNo" id="whatsAppNo" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Tele Phone No</label>
                    <div class="col-lg-3">
                        <select title="Select Country Code" name="phoneCC" id="phoneCCT" class="form-control form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>  
                    </div>
                    <div class="col-lg-4">
                        <input class="form-control form-control-lg form-control-solid number" name="telephoneNo" id="telephoneNo" type="text" />
                    </div>                    
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">We Chat No</label>
                    <div class="col-lg-3">
                        <select title="Select Country Code" name="phoneCC" id="phoneCCWC" class="form-control form-control-solid selectpicker" data-live-search="true">
                            <option value=""></option>
                        </select>  
                    </div>                    
                    <div class="col-lg-4">
                        <input class="form-control form-control-lg form-control-solid number" name="weChatNo" id="weChatNo" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Document Upload</label>
                    <div class="col-lg-2 image-input image-input-outline" id="userImage">
                        <div id="prfImgDiv" class="image-input-wrapper" style="background-image: url()"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change Image">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" id="userImageFile" name="profile_avatar" accept=".png, .jpg, .jpeg"/>
                            <input type="hidden" name="profile_avatar_remove"/>
                        </label>
                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>                                               
            </div>
            <div class="card-footer">
                <button id="btnSendUserData" type="button" onClick="javascript:updateUserData();" class="form-control" style="background-color:#179f00; color:white;font-size:15px; width:30%;">REGISTER</button>
            </div>
            </form>
        </div>
    </div>
</div>