<script src="assets/js/app/login.js"></script>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom" style="width:30%;margin:auto;">
            <div class="card-header" style="align-self: center;">
                <h3 class="card-title">
                    Login
                </h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input id="username" name="username" type="text" class="form-control"  placeholder="Username"/>
                </div>
                <div class="form-group">
                    <input id="userpass" name="userpass" type="password" class="form-control" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <button id="btnLogin" name="btnLogin" onClick="javascript:postLoginReq();" class="form-control" style="background-color:#179f00; color:white;font-size:15px;">SIGN IN</button>
                </div>                
            </div>
            <div class="card-footer" style="text-align:center;">
                <a href="?page=register" style="color:#179f00; font-size:14px;">Register Here</a>
                </br>
                <a href="?page=forgotPassword" style="color:#179f00; font-size:14px;">Forgot Password</a>
            </div>
        </div>
    </div>
</div>