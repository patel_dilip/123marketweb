<script src="assets/js/app/postNewJob.js"></script>
<?php
	if($_GET){
        $postType = $_GET['postType'] ? $_GET['postType'] : '';
        $postId = $_GET['postId'] ? $_GET['postId'] : '';
	}else{
        $postType = '';
        $postId = '';
    }
?>
<script type="text/javascript">
    <?php
        echo "var postType = {$postType};";
        echo "var postId = {$postId};";
    ?>
</script>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom" style="width:60%;margin:auto;">
            <div class="card-header" style="align-self: center;">
                <h3 id="postTitleId" class="card-title">
                    I Want to Sell
                </h3>
            </div>
            <form class="form" id="post_info_form">
            <div class="card-body">
                <div class="row form-group">
                    <label class="col-lg-4 col-form-label">Title <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input class="form-control form-control-lg form-control-solid" name="postTitle" id="postTitle" type="text" />
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-4 col-form-label">Category <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <select id="postCategory" name="postCategory" class="form-control form-control-lg form-control-solid">
                            <option value=""></option>
                        </select> 
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-4 col-form-label">Details <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <textarea class="form-control form-control-lg form-control-solid" id="postDetails" name="postDetails" rows="3"> </textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-3 col-form-label">Upload Image</label>
                    <div class="col-lg-2 image-input image-input-outline" id="postImage">
                        <div id="postImgDiv" class="image-input-wrapper" style="background-image: url()"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change Image">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" id="postImageFile" name="profile_avatar" accept=".png, .jpg, .jpeg"/>
                            <input type="hidden" name="profile_avatar_remove"/>
                        </label>
                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div> 
            </div>
            <div class="card-footer">
                <button type="button" id="btnSubmitLead" name="btnSubmitLead" onClick="javascript:submitPostData();" class="form-control" style="background-color:#179f00; color:white;font-size:15px; width:30%;">SUBMIT LEAD</button>
            </div>
            </form>
        </div>
    </div>
</div>