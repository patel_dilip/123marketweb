<script src="assets/js/app/dashboard.js"></script>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom" style="background-color: transparent;">
            <div class="card-body">
                </br></br></br>
                <!-- Begin -->
                <div class="d-flex flex-center align-items-center">
                    <button type="button" id="postSellingOfferBtn" class="btn btn-outline-dark smFntSize">Post Selling Offers</button>
                    &nbsp;&nbsp;
                    <button type="button" id="postBuyingReqBtn" class="btn btn-outline-dark smFntSize">Post Buying Request</button>
                    &nbsp;&nbsp;
                    <button type="button" id="viewSellingOfferBtn" class="btn btn-outline-dark smFntSize">View Selling Offers</button>
                    &nbsp;&nbsp;
                    <button type="button" id="viewBuyingReqBtn" class="btn btn-outline-dark smFntSize">View Buying Request</button>
                </div>
                </br></br>
                <div class="d-flex flex-center align-items-center">
                    <input class="form-control-lg" style="width: 600px;border: none;" id="searchText" name="searchText" type="text" placeholder="What are you looking for..." />
                    <a href="javascript:viewItemList('Search Result for');" class="btn btn-success btn-shadow font-weight-bold mr-2" style="width: 200px; background-color: #179f00;">Search</a>
                </div>
                </br></br>
                <div id="prdCatLinksDiv" class="d-flex flex-center align-items-center">
                </div>
                </br></br></br></br>
                <div id="prdCatCardDiv" class="content-block">
                </div>
                </br></br></br></br>
                <div style="font-size:26px; text-align: center;">Latest Leads</div>
                </br></br></br></br>
                <div id="latestLeadsDiv" class="d-flex flex-center align-items-center">
                </div>
                </br></br></br></br>
            </div>
        </div>
    </div>
</div>