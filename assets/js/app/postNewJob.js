var postData;
var postImgElem;
var mode = 'add';
var sessionUser;

jQuery(document).ready(function () {
    postImgElem = new KTImageInput('postImage');
    postProfileForm.init();
    if(postId != null && postId != '' && postId !='New')
        mode = 'edit';
    if(userSessionTrue()){
        applyPostData();
    }else{
        redirectToLogin();
    }
    sessionUser = JSON.parse(sessionUserInfo);
});

function applyPostData() {
    applyPostViewSettings();
    loadPostData();
}

function applyPostViewSettings(){
    if(postType == 1){
        if(mode == 'add'){
            $('#postTitleId').text('I Want to Sell');
            $('#btnSubmitLead').text('SUBMIT LEAD');
        }else{
            $('#postTitleId').text('Edit My Sell');
            $('#btnSubmitLead').text('UPDATE LEAD');
        }
    }else{
        if(mode == 'add'){
            $('#postTitleId').text('I Want to Buy');
            $('#btnSubmitLead').text('SUBMIT LEAD');
        }else{
            $('#postTitleId').text('Edit My Buy');
            $('#btnSubmitLead').text('UPDATE LEAD');
        }        
    }
}

function loadPostData(){
    $.ajax({
        type: "POST",
        url: apiURL+"getPostData",
        dataType: 'json',
        data: { 'postId' : postId },      
        success: function(data) {
            postData = data;
            populatePostData();
        }
    }); 
}

function populatePostData(){
    loadSelectList('postCategory',postData.categoryList);

    $('[name="postCategory"]').selectpicker('refresh');

    if(null != postData.postInfo){
        $('#postTitle').val(postData.postInfo.postTitle);
        $('#postCategory').val(postData.postInfo.postCategory);
        $('#postDetails').val(postData.postInfo.postDetails);
        $('#postImgDiv').attr('style','background-image: url(Uploads/'+postData.postInfo.imgPath+')');
        $('[name="postCategory"]').selectpicker('refresh');
    }
}

var postProfileForm = function () {
	var _formEl;
	var _validations = [];
	var _initValidation = function () {
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					postTitle: {
						validators: {
							notEmpty: {
								message: 'Title is required'
							}
						}
					},
					postCategory: {
						validators: {
							notEmpty: {
								message: 'Post Category is required'
							}
						}
                    },
					postDetails: {
						validators: {
							notEmpty: {
								message: 'Post Details are required'
							}
						}
                    },
                },
 				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap({
						eleValidClass: '',
					})
				}
			}
		));
	}
	return {
		init: function () {
			_formEl = KTUtil.getById('post_info_form');
			_initValidation();
		}
	};
}();

function submitPostData(){
    var fd = new FormData(); 
    var files = $('#postImageFile')[0].files[0]; 
    if(null != files && files != ''){
        var newFileName = getFileName(files);
        fd.append('file', files, newFileName); 
        $.ajax({ 
            url: 'upload.php', 
            type: 'post', 
            data: fd, 
            contentType: false, 
            processData: false, 
            success: function(response){ 
                if(response != 0){ 
                   sendPostData(newFileName);
                } 
                else{ 
                    rbAlert('Error in Image Upload'); 
                } 
            }, 
        });
    }else{
        sendPostData(); 
    }
}

function getFileName(files){
    var fileName = "";
    var orFileName = files.name;
    var lastIndxOfPeriod = orFileName.lastIndexOf('.');
    var ext = orFileName.substring(lastIndxOfPeriod);
    var postTitleAbr = $('#postTitle').val().replace(/ /g,'');
    fileName = authToken+postTitleAbr+ext;

    return fileName;
}

function sendPostData(fileName){
    if(validateInputParams()){
        var dataJson = createDataJson(fileName);
        $.ajax({
            type: "POST",
            url: apiURL+"updatePostData",
            data: { 'mode' : mode , 'dataString' : JSON.stringify(dataJson) },
            success: function(data) {
                handleUpdateResponse(data);
            }
        });
    }
}

function handleUpdateResponse(data){
	if(data == "1"){
        if(mode == 'add'){
		    rbAlert("Post Added");
            location.href = '?page=dashboard&authToken='+authToken+'&sessionUserInfo='+sessionUserInfo;  
        }else{
		    rbAlert("Post Updated");
            viewUserProfile();  
        }
	}else{
		rbAlert("Sorry, there is an error. Please contact Admin.");
	}
}

function createDataJson(fileName){
    var params = {};
    params['postId'] = postId;
    if(postType == 1)
        params['postType'] = 'seller';
    else
        params['postType'] = 'buyer';   

    // params['postUser'] = sessionUserInfo;   
    params['postUser'] = sessionUser.userId;

    params['postTitle'] = $('#postTitle').val();
    params['postCategory'] = $('#postCategory').val();
    params['postDetails'] = $('#postDetails').val();
    if(null != fileName && fileName !='')
        params['imgPath'] = fileName;
    return params;
}


function validateInputParams(){
    var resp = false;
    var fieldNames = ['postTitle','postCategory','postDetails'];
    resp = checkIfNonEmptyFields(fieldNames); 
    if(resp != true){
        rbAlert('Field : '+resp+' value is mandatory');
        return false;
    }
    return resp;

}

