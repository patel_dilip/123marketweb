var itemListDataSet = [];
var commonData;
var filterString = {
    keyword:'',
    category:'',
    country:'',
    userType:'',
    postType:''
};
var sessionUser;

jQuery(document).ready(function () {
    loadTitle();
    
    if(null != reqOrigin && reqOrigin != ''){
        if(reqOrigin == 'Selling Offers'){
            filterString.postType = 'seller';
            $('#postTypeS').attr('checked','checked');
        }
        else if(reqOrigin == 'Buying Request'){
            filterString.postType = 'buyer';
            $('#postTypeB').attr('checked','checked');
        }
        else if(reqOrigin == 'Latest Posted Leads'){
            filterString.category = categoryId;
        }        
       else if(reqOrigin == 'Search Result for'){
            filterString.keyword = searchText;
            $('#keyword').val(searchText);
        }
        else if(reqOrigin == 'Latest Posted Leads'){
            filterString.category = categoryId;    
        }
    }
    sessionUser = JSON.parse(sessionUserInfo);
    if(null != sessionUser && sessionUser != '')
        filterString.country = sessionUser.userCountry;
    fetchCommanData();
    fetchLatestLeads();
});

function loadTitle(){
    if(null != reqOrigin && reqOrigin !='')
        $('#itemListTitleSpan').text(reqOrigin);
}

function fetchCommanData(){
    $.ajax({
        type: "POST",
        url: apiURL+"getDomainDataList",
        success: function(data) {
            commonData = JSON.parse(data);
            loadCommonData();
        }
    }); 
}

function loadCommonData(){
    loadSelectList('country',commonData.countryList);
    loadSelectList('category',commonData.categoryList);
    if(userSessionTrue()){
        $('[name="country"]').val(sessionUser.userCountry);
        filterString.country = sessionUser.userCountry;
    }
    $('[name="category"]').val(categoryId);
    $('[name="country"]').selectpicker('refresh');
    $('[name="category"]').selectpicker('refresh');
}

function fetchLatestLeads(){
    $.ajax({
        type: "POST",
        url: apiURL+"getLatestLeads",
        data: { 'filterString' : JSON.stringify(filterString) },
        success: function(data) {
            itemListDataSet = JSON.parse(data);
            loadItemListSection(itemListDataSet, 'itemListDiv');
        }
    }); 
}

function repostRefineSearch(){
    generateFilterData();
    fetchLatestLeads();
}

function generateFilterData(){
    filterString.keyword = $('#keyword').val();
    filterString.category = $('#category').val();
    filterString.country = $('#country').val();

    var postTypeArr = [];
    var userTypeArr = [];
    
    if($("#usrTypeA").is(':checked'))
        userTypeArr.push('Agent');
    if($("#usrTypeT").is(':checked'))
        userTypeArr.push('Trader');    
    if($("#usrTypeM").is(':checked'))
        userTypeArr.push('Manufacturer');  
        
    if($("#postTypeS").is(':checked'))
        postTypeArr.push('seller');
    if($("#postTypeB").is(':checked'))
        postTypeArr.push('buyer');    
    if($("#postTypeBoth").is(':checked'))
        postTypeArr.push('both');         

    filterString.postType = postTypeArr.toString();
    filterString.userType = userTypeArr.toString();



}