jQuery(document).ready(function () {
    loadHeaderSection();
});

function logout(){
    location.href = "?page=dashboard";
}

function viewItemList(source){
    var searchText = $('#searchText').val();
    var pageUrl = "?page=itemList&authToken="+authToken+"&reqOrigin='";
    if(source != null)
        pageUrl += source;
    pageUrl += "'&sessionUserInfo="+sessionUserInfo;
    pageUrl += "&searchText="+searchText;
    location.href = pageUrl;
}

function viewItemListN(source, categoryName){
    var pageUrl = "?page=itemList&authToken="+authToken+"&reqOrigin='";
    if(source != null)
        pageUrl += source;
    pageUrl += "'&sessionUserInfo="+sessionUserInfo;
    pageUrl += "&categoryId="+categoryName;
    location.href = pageUrl;
}

function viewItem(itemId){
    if(userSessionTrue()){
        var pageUrl = "?page=itemView&authToken="+authToken+"&itemId='"+itemId+"'&sessionUserInfo="+sessionUserInfo;
        location.href = pageUrl;
    }else{
        location.href = '?page=login';
    }
}

function userSessionTrue(){
    if(null != authToken && authToken != '')
        return true;
    else
        return false;    
}

function loadHeaderSection(){
    if(userSessionTrue()){
        $('#hdrHomeLinkHref').attr('href','?page=dashboard&authToken='+authToken+'&sessionUserInfo='+sessionUserInfo);
        $('#hdrLoginBtn').attr('style','display:none;');
        $('#hdrRegisterBtn').attr('style','display:none;');
        $('#hdrLogoutBtn').removeAttr('style','display:none;');
        $('#hdrUserDetailsBtn').removeAttr('style','display:none;');
        $('#hdrUserIdSpan').text('Hi '+authToken+', ');
        $('#hdrUserIdLetterSpan').text(authToken.charAt(0));        
    }else{
        $('#hdrHomeLinkHref').attr('href','index.php');
        $('#hdrLoginBtn').removeAttr('style','display:none;');
        $('#hdrRegisterBtn').removeAttr('style','display:none;');
        $('#hdrLogoutBtn').attr('style','display:none;');
        $('#hdrUserDetailsBtn').attr('style','display:none;');       
    }
}

function redirectToLogin(){
    location.href = "?page=login";
}

function viewUserProfile(){
    location.href = '?page=userdetail&authToken='+authToken+'&sessionUserInfo='+sessionUserInfo;
}
