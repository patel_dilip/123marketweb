var itemListDataSet2 = [];
var sessionUser;

jQuery(document).ready(function () {
    sessionUser = JSON.parse(sessionUserInfo);
    fetchUserDetailsAndLeads(sessionUser.userId);
});

function editUserProfile(){
    location.href = "?page=register&authToken="+authToken+"&sessionUserInfo="+sessionUserInfo;
}

function fetchUserDetailsAndLeads(id){
    //latestLeadsDataSet;
    $.ajax({
        type: "POST",
        url: apiURL+"getUserDetailsAndLeads",
        dataType: 'json',
        data: { 'userId' : id },
        success: function(data) {
            itemListDataSet2 = data;
            loadUserItemList();
        }
    }); 
}


function loadUserItemList(){

    var username = itemListDataSet2.userDetailSheet.vchUserName;  
    var userType = itemListDataSet2.userDetailSheet.vchUserType;
    var createdDate = itemListDataSet2.userDetailSheet.dtCreatedDate;
    var usrImgPath = 'Uploads/'+itemListDataSet2.userDetailSheet.vchFileName;

    $('#userProfileName').append(authToken);
    $('#userType').append(userType); 
    $('#dateRegisterdOn').append(createdDate);
  
    $('#profileImgDiv').attr('style','background-image: url('+usrImgPath+')');

    var itemGrid="";
    var border = '<br><div class="border" style="border:2px solid grey; box-sizing: border-box; border-radius:5px;"><br>';
    var row ='<div class="form-group row">';
    var imgSec='<div class="col-lg-2">';
    // after this we need to give img src path dynamically;
    var endDiv='</div>';
    var decSec='<div class="col-lg-6">';
    // after this we need to give description dynamically;
        endDiv;
   // var IconAndButton='<div class="col-lg-2"> <i style="float:left;" class="icon-2x text-dark-50 flaticon2-edit"></i><i style="float:right;" class="icon-2x text-dark-50 flaticon2-rubbish-bin"></i></div><div class="col-lg-2"><button type="button" class="btn btn-primary" style="background-color: #179f00; align:center;">View Item</button></div></div></div><br>';    
    //var all = '<div class="col-lg-2"> <i onClick="editPostItem();" style="float:left;" class="icon-2x text-dark-50 flaticon2-edit"></i><i style="float:right;" class="icon-2x text-dark-50 flaticon2-rubbish-bin"></i></div><div class="col-lg-2"><button type="button" class="btn btn-primary" style="background-color: #179f00; align:center;">View Item</button></div></div></div><br>'; 
    var editIconInit ='<div class="col-lg-2"> <i onClick="editPostItem(';    
    var editIconEnd = ');" style="float:left;" class="icon-2x text-dark-50 flaticon2-edit"></i>';
    var delIconInit = '<i style="float:right;" class="icon-2x text-dark-50 flaticon2-rubbish-bin" onClick="deletePostItem(';
    var delIconEnd = ')"></i></div>';
    var viewIconInit = '<div class="col-lg-2"><button type="button" class="btn btn-primary" style="background-color: #179f00; align:center;" onClick="viewPostItem(';
    var viewIconEnd = ')">View Item</button></div></div></div><br>';

    itemGrid +='';
    for(var i=0; i<itemListDataSet2.leadDetails.length; i++)
    {
        var inputImag=itemListDataSet2.leadDetails[i].vchFileName;
        var descri=itemListDataSet2.leadDetails[i].vchDescriptions;
        var postIdValue =itemListDataSet2.leadDetails[i].intProductId;
        var postTypeValue =itemListDataSet2.leadDetails[i].vchUserType

        itemGrid += border;
        itemGrid += row;
        itemGrid += imgSec;
        itemGrid += '<img alt="Pic" src="Uploads/'+inputImag+'">';
        itemGrid += endDiv;
        itemGrid += decSec;
        itemGrid += descri;
        itemGrid += endDiv;
        itemGrid += editIconInit;
        itemGrid += postIdValue+',\''+postTypeValue+'\'';
        itemGrid += editIconEnd;
        itemGrid += delIconInit;
        itemGrid += postIdValue;
        itemGrid += delIconEnd;
        itemGrid += viewIconInit;
        itemGrid += postIdValue;
        itemGrid += viewIconEnd;
    }
    $('#userLeadDetailsCard').append(itemGrid);  
}

function editPostItem(postId, strPostType){
    var intPostType;
    if(strPostType == 'seller')
        intPostType = 1;
    else
        intPostType = 2;
    var postUrl = '?page=postNewJob&authToken='+authToken
                    +'&postType='+intPostType
                    +'&postId='+postId
                    +'&sessionUserInfo='+sessionUserInfo
                    ;
    location.href = postUrl;
}

function deletePostItem(postId){
    //rbAlert('Delete Called for Post ID : '+postId);
    $("#confirmModalLabel").text('Confirm Delete');
    $("#confirmModalBody").text('Please confirm to delete this Post from Leads');
    
    $('#confirmButtonId').attr('onClick','removeData(\''+postId+'\')');
    $("#confirmModal").modal('show');
}

function removeData(postId){
    $.ajax({
        type: "POST",
        url: apiURL+"deletePostData",
        data: { 'postId' : postId },
        success: function(data) {
            handleDeleteResponse();
        }
    });     
}

function handleDeleteResponse(){
    viewUserProfile();
}

function viewPostItem(postId){
    viewItem(postId);
}






                                                    