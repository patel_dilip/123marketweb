var prdCatDataSet = [
    {imgUrl : 'assets/media/productCat/Image_1.jpg', catName : 'Coveralls / PPE Kits'},
    {imgUrl : 'assets/media/productCat/Image_2.jpg', catName : 'Goggles / Face Shields'},
    {imgUrl : 'assets/media/productCat/Image_3.jpg', catName : 'Gloves'},
    {imgUrl : 'assets/media/productCat/Image_4.jpg', catName : 'Goggles / Face Shields'},
    {imgUrl : 'assets/media/productCat/Image_5.jpg', catName : 'SHOE / HEAD COVER'},
    {imgUrl : 'assets/media/productCat/Image_6.jpg', catName : 'Mask'},
    {imgUrl : 'assets/media/productCat/Image_7.jpg', catName : 'THERMO METERS'},
    {imgUrl : 'assets/media/productCat/Image_8.jpg', catName : 'Oximeters'},
    {imgUrl : 'assets/media/productCat/Image_9.jpg', catName : 'Covid 19 Testing Kits'},
    {imgUrl : 'assets/media/productCat/Image_10.jpg', catName : 'SHOE / HEAD COVER'},
    {imgUrl : 'assets/media/productCat/Image_11.jpg', catName : 'Mask'},
    {imgUrl : 'assets/media/productCat/Image_12.jpg', catName : 'Other'}                                        
];

var prdCatLinkDataSet = [
    {link : '#', catName : 'Coveralls / PPE Kits'},
    {link : '#', catName : 'Goggles / Face Shields'},
    {link : '#', catName : 'Gloves'},
    {link : '#', catName : 'THERMO METERS'},
    {link : '#', catName : 'Oximeters'},
    {link : '#', catName : 'Covid 19 Testing Kits'},
    {link : '#', catName : 'SHOE / HEAD COVER'},
    {link : '#', catName : 'Mask'},
    {link : '#', catName : 'Other'}                                        
];

var latestLeadsDataSet = [];

jQuery(document).ready(function () {
    loadPrdCatLinkSection(prdCatLinkDataSet, 'prdCatLinksDiv');
    loadProductCatCardSection(prdCatDataSet, 'prdCatCardDiv');
    fetchLatestLeads();
    loadUserDashboardSettings();
});

function fetchLatestLeads(){
    $.ajax({
        type: "POST",
        url: apiURL+"getLatestLeads",
        data: { 'filterString' : '' },
        success: function(data) {
            latestLeadsDataSet = JSON.parse(data);
            loadLatestLeadsSection(latestLeadsDataSet,'latestLeadsDiv');
        }
    }); 
}

function loadUserDashboardSettings(){
    if(userSessionTrue()){
        $('#postSellingOfferBtn').attr('onClick',"javascript:location.href = '?page=postNewJob&authToken="+authToken+"&postType=1&postId=\"New\"&sessionUserInfo="+sessionUserInfo+"'");
        $('#postBuyingReqBtn').attr('onClick',"javascript:location.href = '?page=postNewJob&authToken="+authToken+"&postType=2&postId=\"New\"&sessionUserInfo="+sessionUserInfo+"'");
        $('#viewSellingOfferBtn').attr('onClick',"viewItemList('Selling Offers')");
        $('#viewBuyingReqBtn').attr('onClick',"viewItemList('Buying Request')");
    }else{
        $('#postSellingOfferBtn').attr('onClick',"javascript:location.href = '?page=login'");
        $('#postBuyingReqBtn').attr('onClick',"javascript:location.href = '?page=login'");
        $('#viewSellingOfferBtn').attr('onClick',"javascript:location.href = '?page=login'");
        $('#viewBuyingReqBtn').attr('onClick',"javascript:location.href = '?page=login'");
    }
}