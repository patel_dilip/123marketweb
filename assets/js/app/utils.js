function rbAlert(message){
	Swal.fire({
		text: message,
		icon: "error",
		buttonsStyling: false,
		confirmButtonText: "Ok",
		customClass: {
			confirmButton: "btn font-weight-bold btn-primary",
		}
	});
}

function rbMessage(message){
	Swal.fire({
		text: message,
        icon: "success",
        type: 'success',
		buttonsStyling: false,
		confirmButtonText: "Ok",
		customClass: {
			confirmButton: "btn font-weight-bold btn-primary",
		}
	});
}

function loadLatestLeadsSection(latestLeadsDataSet,latestLeadsDiv){
    var latestLeadsElem = "";
    var elemInit = '<table style="width:80%;border-collapse:collapse;"><tbody>';
    var rowInit = '<tr><td><div class="row"><div class="col"><div class="card p-3"><div class="row">';
    var rowEnd = '</div></div></div></div></td></tr>';
    var elemEnd = '</tbody></table>';

    latestLeadsElem += elemInit;
    for(var cnt=0; cnt < latestLeadsDataSet.length; cnt++){

        //var linkStr = '#';//latestLeadsDataSet[cnt].link;
        var linkStr = 'javascript:viewItem('+latestLeadsDataSet[cnt].intProductId+');';
        var imgurlStr = latestLeadsDataSet[cnt].ImgPath;
        var postNameStr = latestLeadsDataSet[cnt].vchTitle;
        var usertypeStr = latestLeadsDataSet[cnt].vchUserType;
        var phonenoStr = latestLeadsDataSet[cnt].vchWhatsAppNo;
        var mailidStr = latestLeadsDataSet[cnt].vchUserID;

        latestLeadsElem += rowInit;

        latestLeadsElem += '<div class="col col-xs-12 col-md-2">';
        latestLeadsElem += '<a href="'+linkStr+'">';
        latestLeadsElem += '<img src="'+imgurlStr+'" width="50" height="50">';
        latestLeadsElem += '</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-3">';
        latestLeadsElem += '<a href="'+linkStr+'" style="color: #179f00; font-size: 15px;">';
        latestLeadsElem += postNameStr;
        latestLeadsElem += '</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-2" style="font-size: 15px;">';
        latestLeadsElem += usertypeStr;
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-1">';
        latestLeadsElem += '<a href="https://api.whatsapp.com/send?phone='+phonenoStr+'" target="_blank">';
        latestLeadsElem += '<i class="fab fa-whatsapp-square" style="font-size: 36px; color: #25d366"></i>';
        latestLeadsElem += '</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-1">';
        latestLeadsElem += '<a href="tel:'+phonenoStr+'">';
        latestLeadsElem += '<i class="fa fa-phone" style="font-size: 26px; color: #000000"></i>';
        latestLeadsElem += '</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-1">';
        latestLeadsElem += '<a href="mailto:'+mailidStr+'" class="top-mail">';
        latestLeadsElem += '<i class="fa fa-envelope" style="font-size: 36px; color: #1a73e8"></i>';
        latestLeadsElem += '</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += '<div class="col col-xs-12 col-md-2">';
        latestLeadsElem += '<a class="btn btn-primary" style="font-size: 15px; background-color: #179f00" href="'+linkStr+'">View Item</a>';
        latestLeadsElem += '</div>';

        latestLeadsElem += rowEnd;
    }
    latestLeadsElem += elemEnd;

    $('#'+latestLeadsDiv).append(latestLeadsElem);  
}

function loadProductCatCardSection(prdCatDataSet, prdCatCardDiv){
    var prdCatCard = "";
    var rowinit = '<div class="row d-flex flex-center align-items-center">';
    var colinit = '<div class="col-xl-2">';
    var enddiv = '</div>';
    var imgElemDiv = '<div class="card card-custom bgi-no-repeat bgi-no-repeat bgi-size-cover gutter-b" style="height: 230px; background-image: url(';
    var descElemDiv = '<div class="" style="font-size:16px">';
    var descElemA1 = '<a href="javascript:viewItemListN(\'Latest Posted Leads\'';
    var descElemA2 = ');" class="text-dark-75 text-hover-primary font-weight-bolder font-size-h3">';

    for(var cnt=0; cnt < prdCatDataSet.length; cnt++){
        if(cnt % 4 == 0)
            prdCatCard += rowinit;
        prdCatCard += colinit;
            prdCatCard += descElemA1;
            prdCatCard += ',\''+prdCatDataSet[cnt].catName+'\'';
            prdCatCard += descElemA2;
            prdCatCard += descElemDiv + prdCatDataSet[cnt].catName+enddiv;
            prdCatCard += imgElemDiv + prdCatDataSet[cnt].imgUrl + ')">'+enddiv;
            prdCatCard += '</a>';
        prdCatCard += enddiv;        
        if(cnt > 2 && (cnt+1)%4 == 0)
            prdCatCard += enddiv;
    }

    $('#'+prdCatCardDiv).append(prdCatCard);    
}

function loadPrdCatLinkSection(prdCatLinkDataSet, prdCatLinksDiv){
    var topWrapElem = '<div class="text-center"><ul>';
    var liItemDiv = '<li class="list-inline-item">';
    //var linkItem = '<a class="btn btn-outline-dark smFntSize" href="';
    var linkItem = '<a class="btn btn-outline-dark smFntSize" href="javascript:viewItemListN(\'Latest Posted Leads\'';
    var endLinkItem = '</a></li>';
    var endWrapElem = '</ul></div>';

    var prdCatLinkDivElem = topWrapElem;
    for(var cnt=0; cnt < prdCatLinkDataSet.length; cnt++){
        prdCatLinkDivElem += liItemDiv;
        //prdCatLinksDiv += linkItem+prdCatLinkDataSet[cnt].link+'">';
        prdCatLinkDivElem += linkItem;
        prdCatLinkDivElem += ',\''+prdCatLinkDataSet[cnt].catName+'\');" >';
        prdCatLinkDivElem += prdCatLinkDataSet[cnt].catName;
        prdCatLinkDivElem += endLinkItem;
    }
    prdCatLinkDivElem += endWrapElem;

    $('#'+prdCatLinksDiv).append(prdCatLinkDivElem); 
}

function loadItemListSection(itemListDataSet, itemListDiv){
    var itemListElem = "";
    var rowInit = '<div class="row">';
    var cardInit = '<div class="col-lg-4"><div class="card card-custom">';
    var headerInit = '<div class="card-header"><div class="card-title" style="margin:auto;">';
    var hrefInit = '<a href="';
    var titleInit = '<h3 class="card-label">';
    var titleEnd = '</h3>';
    var hrefEnd = '</a>';
    var headerEnd = '</div></div>';

    var bodyInit = '<div class="card-body" style="margin:auto;cursor: pointer;"><div class="symbol symbol-150 mr-3">';
    var bodyEnd = '</div></div>';
    var cardEnd = '</div></div>';
    var rowEnd = '</div></br>';

    itemListElem += '';
    if(itemListDataSet.length > 0){
        for(var cnt=0; cnt < itemListDataSet.length; cnt++){
            var imgurlStr = itemListDataSet[cnt].ImgPath;
            var postNameStr = itemListDataSet[cnt].vchTitle;
            if(cnt % 3 == 0)
                itemListElem += rowInit;
            itemListElem += cardInit;
                itemListElem += headerInit;
                    itemListElem += hrefInit;
                        itemListElem += 'javascript:viewItem('+itemListDataSet[cnt].intProductId+');">';
                        itemListElem += titleInit;
                            itemListElem += postNameStr;
                        itemListElem += titleEnd;
                    itemListElem += hrefEnd;
                itemListElem += headerEnd;
                itemListElem += bodyInit;
                    itemListElem += '<img onClick="javascript:viewItem('+itemListDataSet[cnt].intProductId+');" alt="Pic" src="'+imgurlStr+'">';
                itemListElem += bodyEnd;
            itemListElem += cardEnd;
            if(cnt > 1 && (cnt+1)%3 == 0)
                itemListElem += rowEnd;
        }
    }else{
        itemListElem += '<div class="row card" style="text-align:center;height:50%;font-size:14px;font-weight:bold;">';
        itemListElem += "</br>No Records Found!";
        itemListElem += '</div>';

    }
    $('#'+itemListDiv).empty();
    $('#'+itemListDiv).append(itemListElem);  
}

function loadSelectList(selectId,data){
    $('[name="'+selectId+'"]').empty();
    $('[name="'+selectId+'"]').append(new Option('', ''));
    for(var cnt=0; cnt<data.length; cnt++){
        $('[name="'+selectId+'"]').append(new Option(data[cnt].name, data[cnt].id));
	}
}

function loadPhoneCCList(selectId,data){
    $('[name="'+selectId+'"]').empty();
    $('[name="'+selectId+'"]').append(new Option('', ''));
    for(var cnt=0; cnt<data.length; cnt++){
        $('[name="'+selectId+'"]').append(new Option('('+data[cnt].phoneCC+') '+data[cnt].country, data[cnt].phoneCC));
	}
}

function loadDataList(selectId,data){
	if(null != data && data != ''){
		$('#'+selectId).empty();
		$('#'+selectId).append(new Option('', ''));
		for(var cnt=0; cnt<data.length; cnt++){
			$('#'+selectId).append(new Option(data[cnt].id, data[cnt].name));
		}
	}
}

function isFieldValueEmpty(selectId, type){
    var resp = true;
    var value = null;
    if(type=="name")
        value = $('[name="'+selectId+'"]').val();
    else
        value = $('#'+selectId).val();
    if(null != value && value != '')
        resp = false;
    return resp;  
}

function checkIfNonEmptyFields(fieldNames){
    var resp = true;
    var errorField = '';
    for(var cnt=0; cnt<fieldNames.length; cnt++){
        var value = $('#'+fieldNames[cnt]).val();
        if(null == value || value == ''){
            resp = false;
            errorField = fieldNames[cnt];
            break;
        }
    }
    if(resp == false)
        return errorField;  
    else
        return true;
}

function SHA1(msg){function rotate_left(n,s){var t4=(n<<s)|(n>>>(32-s));return t4;};function lsb_hex(val){var str='';var i;var vh;var vl;for(i=0;i<=6;i+=2){vh=(val>>>(i*4+4))&0x0f;vl=(val>>>(i*4))&0x0f;str+=vh.toString(16)+vl.toString(16);}
return str;};function cvt_hex(val){var str='';var i;var v;for(i=7;i>=0;i--){v=(val>>>(i*4))&0x0f;str+=v.toString(16);}
return str;};function Utf8Encode(string){string=string.replace(/\r\n/g,'\n');var utftext='';for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if(c<128){utftext+=String.fromCharCode(c);}
else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}
else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}
return utftext;};var blockstart;var i,j;var W=new Array(80);var H0=0x67452301;var H1=0xEFCDAB89;var H2=0x98BADCFE;var H3=0x10325476;var H4=0xC3D2E1F0;var A,B,C,D,E;var temp;msg=Utf8Encode(msg);var msg_len=msg.length;var word_array=new Array();for(i=0;i<msg_len-3;i+=4){j=msg.charCodeAt(i)<<24|msg.charCodeAt(i+1)<<16|msg.charCodeAt(i+2)<<8|msg.charCodeAt(i+3);word_array.push(j);}
switch(msg_len % 4){case 0:i=0x080000000;break;case 1:i=msg.charCodeAt(msg_len-1)<<24|0x0800000;break;case 2:i=msg.charCodeAt(msg_len-2)<<24|msg.charCodeAt(msg_len-1)<<16|0x08000;break;case 3:i=msg.charCodeAt(msg_len-3)<<24|msg.charCodeAt(msg_len-2)<<16|msg.charCodeAt(msg_len-1)<<8|0x80;break;}
word_array.push(i);while((word_array.length % 16)!=14)word_array.push(0);word_array.push(msg_len>>>29);word_array.push((msg_len<<3)&0x0ffffffff);for(blockstart=0;blockstart<word_array.length;blockstart+=16){for(i=0;i<16;i++)W[i]=word_array[blockstart+i];for(i=16;i<=79;i++)W[i]=rotate_left(W[i-3]^W[i-8]^W[i-14]^W[i-16],1);A=H0;B=H1;C=H2;D=H3;E=H4;for(i=0;i<=19;i++){temp=(rotate_left(A,5)+((B&C)|(~B&D))+E+W[i]+0x5A827999)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
for(i=20;i<=39;i++){temp=(rotate_left(A,5)+(B^C^D)+E+W[i]+0x6ED9EBA1)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
for(i=40;i<=59;i++){temp=(rotate_left(A,5)+((B&C)|(B&D)|(C&D))+E+W[i]+0x8F1BBCDC)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
for(i=60;i<=79;i++){temp=(rotate_left(A,5)+(B^C^D)+E+W[i]+0xCA62C1D6)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
H0=(H0+A)&0x0ffffffff;H1=(H1+B)&0x0ffffffff;H2=(H2+C)&0x0ffffffff;H3=(H3+D)&0x0ffffffff;H4=(H4+E)&0x0ffffffff;}
var temp=cvt_hex(H0)+cvt_hex(H1)+cvt_hex(H2)+cvt_hex(H3)+cvt_hex(H4);return temp.toLowerCase();}