var profileData;
var userImgElem;
var mode = 'add';

jQuery(document).ready(function () {
    if(userSessionTrue()){
        //alert('User Already Registered'); // set data, AND SET ne TO USERNAME
        applyUserData();
    }else{
        fetchProfileData(' ');
    }
    userImgElem = new KTImageInput('userImage');
    userProfileForm.init();
});

function applyUserData(){
    $('#pageTitleUserInfo').text("Edit User Profile");
    $('#btnSendUserData').text("UPDATE");
    $('#username').attr('disabled','disabled');
    $('#passwordDiv').attr('style','display:none;');
    $('#username').val(authToken);

    mode = 'edit';
    fetchProfileData(authToken);
}

function fetchProfileData(userId){
    $.ajax({
        type: "POST",
        url: apiURL+"getUserProfileData",
        dataType: 'json',
        data: { 'userId' : userId },      
        success: function(data) {
            profileData = data;
            loadProfileData();
        }
    }); 
}

function loadProfileData(){
    loadSelectList('country',profileData.countryList);
    //loadSelectList('state',profileData.stateList);
    //loadSelectList('city',profileData.cityList);
    loadDataList('stateList',profileData.stateList);
    loadDataList('cityList',profileData.cityList);
    loadPhoneCCList('phoneCC',profileData.phoneCC);

    $('[name="country"]').selectpicker('refresh');
    //$('[name="state"]').selectpicker('refresh');
    //$('[name="city"]').selectpicker('refresh');
    $('[name="phoneCC"]').selectpicker('refresh');

    $('.number').on('input', function(e) { 
        $(this).val($(this).val().replace(/[^0-9]/g, '')); 
    });

    if(null != profileData.userProfile){
        $('#userType').val(profileData.userProfile.userType);
        $('#companyName').val(profileData.userProfile.companyName);
        $('#products').val(profileData.userProfile.products);
        $('#address').val(profileData.userProfile.address);
        $('#pincode').val(profileData.userProfile.pincode);
        $('#country').val(profileData.userProfile.country);
        $('#state').val(profileData.userProfile.state);
        $('#city').val(profileData.userProfile.city);
        $('#contactPerson').val(profileData.userProfile.contactPerson);
        $('#mobileNo').val(profileData.userProfile.mobileNo);
        $('#whatsAppNo').val(profileData.userProfile.whatsAppNo);
        $('#telephoneNo').val(profileData.userProfile.telephoneNo);
        $('#weChatNo').val(profileData.userProfile.weChatNo);
        $('#phoneCCM').val(profileData.userProfile.phoneCCM);
        $('#phoneCCWA').val(profileData.userProfile.phoneCCWA);
        $('#phoneCCT').val(profileData.userProfile.phoneCCT);
        $('#phoneCCWC').val(profileData.userProfile.phoneCCWC);

        $('#prfImgDiv').attr('style','background-image: url(Uploads/'+profileData.userProfile.imgPath+')');

    }
    $('[name="country"]').selectpicker('refresh');

}

function validateInputParams(){
    var resp = false;

    var fieldNames = ['username','password','userType','companyName','country','city','mobileNo','phoneCCM'];
    resp = checkIfNonEmptyFields(fieldNames); // && !isFieldValueEmpty('phoneCC','name');
    if(resp != true){
        rbAlert('Field : '+resp+' value is mandatory');
        return false;
    }
    return resp;

}

function getFileName(files){
    var fileName = "";
    var userName = $('[name="username"]').val();

    var orFileName = files.name;
    var lastIndxOfPeriod = orFileName.lastIndexOf('.');
    var ext = orFileName.substring(lastIndxOfPeriod);

    fileName = userName+ext;

    return fileName;
}

function updateUserData(){
    var fd = new FormData(); 
    var files = $('#userImageFile')[0].files[0]; 
    if(null != files && files != ''){
        var newFileName = getFileName(files);
        fd.append('file', files, newFileName); 

        $.ajax({ 
            url: 'upload.php', 
            type: 'post', 
            data: fd, 
            contentType: false, 
            processData: false, 
            success: function(response){ 
                if(response != 0){ 
                //alert('file uploaded' + response); 
                submitUserData(newFileName);
                } 
                else{ 
                    rbAlert('Error in File Upload'); 
                } 
            }, 
        });
    }else{
        submitUserData(); 
    }
}

function submitUserData(response){
    //var mode = 'add';
    if(validateInputParams()){
        var dataJson = createDataJson(response);
        $.ajax({
            type: "POST",
            url: apiURL+"updateUserProfileData",
            data: { 'mode' : mode , 'dataString' : JSON.stringify(dataJson) },
            success: function(data) {
                handleUpdateResponse(data);
            }
        });
    }
}

function handleUpdateResponse(data){
	if(data == "1"){
        if(mode == 'add'){
		    rbAlert("User profile updated. Please Login.");
            location.href = "?page=login";  
        }else{
		    rbAlert("User profile updated.");
            viewUserProfile();  
        }
	}else{
		rbAlert("Sorry, there is an error. Please contact Admin.");
	}
}

function createDataJson(response){

    var params = {};

    params['username'] = $('#username').val();
    params['password'] = $('#password').val();
    params['userType'] = $('#userType').val();
    params['companyName'] = $('#companyName').val();
    params['products'] = $('#products').val();
    params['address'] = $('#address').val();
    params['pincode'] = $('#pincode').val();
    params['country'] = $('#country').val();
    params['state'] = $('#state').val();
    params['city'] = $('#city').val();
    params['contactPerson'] = $('#contactPerson').val();
    params['mobileNo'] = $('#mobileNo').val();
    params['whatsAppNo'] = $('#whatsAppNo').val();
    params['telephoneNo'] = $('#telephoneNo').val();
    params['weChatNo'] = $('#weChatNo').val();
    params['phoneCCM'] = $('#phoneCCM').val();
    params['phoneCCWA'] = $('#phoneCCWA').val();
    params['phoneCCT'] = $('#phoneCCT').val();
    params['phoneCCWC'] = $('#phoneCCWC').val();
    if(null != response && response !='')
        params['imgPath'] = response;

    return params;
}

var userProfileForm = function () {
	// Base elements
	var _formEl;
	var _validations = [];

	// Private functions
	var _initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					username: {
						validators: {
							notEmpty: {
								message: 'Username is required'
							}
						}
					},
					password: {
						validators: {
							notEmpty: {
								message: 'Password is required'
							}
						}
                    },
					companyName: {
						validators: {
							notEmpty: {
								message: 'Company Name is required'
							}
						}
                    },
					country: {
						validators: {
							notEmpty: {
								message: 'Country is required'
							}
						}
                    },
					city: {
						validators: {
							notEmpty: {
								message: 'City is required'
							}
						}
                    },
					mobileNo: {
						validators: {
							notEmpty: {
								message: 'Mobile No. is required'
							}
						}
					},                                                                                                    		
                },
 				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));

	}

	return {
		// public functions
		init: function () {
			_formEl = KTUtil.getById('user_profile_form');
			_initValidation();
		}
	};
}();