var latestLeadsDataSet = [];
var itemDetails = {};

jQuery(document).ready(function () {
    fetchItemDetails(itemId);
    fetchLatestLeads();
});

function fetchItemDetails(itemId){
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: { 'itemId' : itemId },       
        url: apiURL+"getItemDetails",
        success: function(data) {
            itemDetails = data;
            loadItemDetails();
        }
    }); 
}

function loadItemDetails(){
    $('#itemName').text(itemDetails.itemName);
    $('#itemDescription').text(itemDetails.itemDescription);
    $('#userName').text(itemDetails.userName);
    $('#userAddress').text(itemDetails.userAddress);
    $('#userPhoneWA').text(itemDetails.userPhoneWA);
    $('#userPhone').text(itemDetails.userPhone);
    $('#userEmail').text(itemDetails.userEmail);
    $('#userPostedOn').text(itemDetails.userPostedOn);
    $('#userType').text(itemDetails.userType);
    $('#itemImg').attr("src",itemDetails.itemImg);
    $('#userImg').attr("style","background-image: url("+itemDetails.userImg+")");
    $('#userPhoneWAhref').attr("href","https://api.whatsapp.com/send?phone="+itemDetails.userPhoneWA);
    $('#userPhonehref').attr("href","tel:"+itemDetails.userPhone);
    $('#userEmailhref').attr("href","mailto:"+itemDetails.userEmail);
}

function fetchLatestLeads(){
    $.ajax({
        type: "POST",
        url: apiURL+"getLatestLeads",
        success: function(data) {
            latestLeadsDataSet = JSON.parse(data);
            loadLatestLeadsSection(latestLeadsDataSet,'latestLeadsDiv');
        }
    }); 
}