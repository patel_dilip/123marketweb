function postLoginReq(){
    var userId = $('#username').val();
    var userPass = $('#userpass').val();

    if((null != userId && userId != '') && (null != userPass && userPass != '')){
        var dataJson = {'username':userId,'password':userPass};
        var accessToken = SHA1(userId);
        $.ajax({
            type: "POST",
            url: apiURL+"validateUserInfo",
            data: { 'userInfo' : JSON.stringify(dataJson) },
            success: function(data) {

//              if(data > 0){
//                  rbMessage("User Authenticated");
//                  location.href = "?page=dashboard&sessionUserInfo="+data+"&authToken="+userId+"&accessToken="+accessToken;

                if(data != null && data != '' && data != 'Error'){
                    rbMessage("User Authenticated");
                    location.href = "?page=dashboard&sessionUserInfo="+data+"&authToken="+userId+"&accessToken="+accessToken;

                }else{
                    rbAlert('Invalid Credentials!');
                }
            }
        });
    }else{
        rbAlert('User Name or Password cannot be blank!');
    }
}