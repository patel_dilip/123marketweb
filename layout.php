<?php // routing controller
	if($_GET){
		$route = $_GET['page'] ? 'pages/app/'.$_GET['page'].'.php' : 'pages/app/dashboard.php';
		if(isset($_GET['authToken'])){
			$authToken = $_GET['authToken'] ? $_GET['authToken'] : '';
		}else{
			$authToken = '';
		}
		if(isset($_GET['sessionUserInfo'])){
			$sessionUserInfo = $_GET['sessionUserInfo'] ? $_GET['sessionUserInfo'] : '';
		}else{
			$sessionUserInfo = '';
		}		
	}else{
		$route = 'pages/app/dashboard.php';
		$authToken = '';
		$sessionUserInfo = '';
	}
?>
<script type="text/javascript">
    <?php
		echo "var authToken = '{$authToken}';";
		echo "var sessionUserInfo = '{$sessionUserInfo}';";
    ?>
</script>
<!--begin::Main-->

<!--[html-partial:include:{"file":"partials/_header-mobile.html"}]/-->
		<!--?php include("partials/_header-mobile.html"); ?-->

		<div class="d-flex flex-column flex-root">

	

			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">

				<!--[html-partial:include:{"file":"partials/_aside.html"}]/-->
				<!--?php include("partials/_aside.html"); ?-->

				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
				<?php include("partials/_header.html"); ?>	
					<!--[html-partial:include:{"file":"partials/_header.html"}]/-->
					<!--?php include("partials/_header.html"); ?-->

					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

						<!--[html-partial:include:{"file":"partials/_content.html"}]/-->
						<?php include($route); ?>
					</div>

					<!--end::Content-->

					<!--[html-partial:include:{"file":"partials/_footer.html"}]/-->
					
				</div>
				
				<!--end::Wrapper-->
			</div>
			<?php include("partials/_footer.html"); ?>
			<!--end::Page-->
		</div>

		<!--end::Main-->
		<script src="assets/js/app/utils.js"></script>
		<script src="assets/js/app/layout.js"></script>